# Typora Support

## Quick Start -> [Markdown Reference](http://support.typora.io/Markdown-Reference/)

> 简介`Markdown`基本语法, 及`Typora`的特性.
> **Markdown**由[Daring Fireball](http://daringfireball.net/)创建；原始指南在[这里](http://daringfireball.net/projects/markdown/syntax)。但是，其语法在不同的解析器或编辑器之间有所不同。**Typora**尝试遵循[GitHub ](https://help.github.com/articles/github-flavored-markdown/)**Flavored** [Markdown](https://help.github.com/articles/github-flavored-markdown/)，但可能仍然存在一些不兼容之处。

## Quick Start -> [Install and Use Pandoc](http://support.typora.io/Install-and-Use-Pandoc/)

> Typora需要[Pandoc](http://pandoc.org/)才能使用某些高级功能。
> [Pandoc](http://pandoc.org/) 是通用文档文本转换器。Typora使用它来支持几种文件类型的文件导入/导出功能。

## Quick Start -> [Images in Typora](http://support.typora.io/Images/)

> 由于Markdown文件是纯文本文件，因此您不能直接将图像数据插入Markdown文件中，而是插入对图像文件的*引用*。
>
> 在Markdown中，图像的书写方式为`![alt](src)`。在`src`这里可以像一个URL `https://octodex.github.com/images/yaktocat.png`，或绝对/相对文件路径，像`../images/test.png`。

## How-Tos -> [Upload Images](http://support.typora.io/Upload-Image/)

> 在较新版本的Typora中（在MacOS上为0.9.9.32或在Windows / Linux上为0.9.84），我们添加了“上传图像”功能，可通过第3个应用程序或脚本将图像上传到云图像存储。
>
> 它的动机是，由于markdown文件只是纯文本文件，因此在嵌入图像时，markdown文件不会“拥有”这些图像，而只是对使用的外部图像文件保持弱引用。当您移动或共享降价文件时，这些图像也应被移动或共享，这带来了维护成本。但是，如果这些图像在线托管，则可以自由移动或共享降价文件，而无需在纯文本和所用图像之间保持引用。
>
> Typora现在支持iPic，uPic，PicGo等应用程序，这些应用程序可以将图像上传到[Imgur](http://imgur.com/)，[Flickr](https://www.flickr.com/)，[Amazon S3](https://aws.amazon.com/s3/)，[Github](https://support.typora.io/Upload-Image/www.github.com)或其他图像托管服务。

### Configuration

You can enable and config image uploader in preferences panel, we provide integrations with following apps / tools:

- iPic (macOS)
- uPic (macOS)
- PicGo.app (macOS / Windows / Linux, Simplified Chinese language only)
- PicGo (Command Line) (Windows / Linux)
- Custom (macOS / Windows / Linux)