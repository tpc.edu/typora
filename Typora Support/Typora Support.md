## Typora Support

### Quick Start

#### [Markdown Reference](Quick Start/Markdown Reference.md "MARKDOWN, TUTORIAL")

#### [Install and Use Pandoc](Quick Start/Install and Use Pandoc.md "PANDOC, TUTORIAL, DOCX")

#### [Draw Diagrams With Markdown](Quick Start/Draw Diagrams With Markdown.md "MARKDOWN, MERMAID, FLOWCHART, SEQUENCE")

#### [About Themes](Quick Start/About Themes.md "STYLE, TUTORIAL")

#### [Install Typora on Linux](Quick Start/install Typora on Linux.md "LINUX, TUTORIAL")

#### [Images in Typora](Quick Start/Images in Typora.md "MARKDOWN, TUTORIAL, IMAGE")

#### [Table Editing](Quick Start/Table Editing.md "MARKDOWN, TUTORIAL, TABLE")
#### [Report Bugs](Quick Start/Report Bugs.md "AUTO-SAVE, HISTORY, VERSION")
#### [Links](Quick Start/Links.md "MARKDOWN, TUTORIAL, LINKS")

#### [Math and Academic Functions](Quick Start/Math and Academic Functions.md "MATH, LATEX")

#### [HTML Support in Typora](Quick Start/HTML Support in Typora.md "HTML, VIDEO")

#### [Spellcheck](Quick Start/Spellcheck.md "SPELLCHECK, I18N")

#### [Shortcut Keys](Quick Start/Shortcut Keys.md "SHORTCUT KEYS")

#### [File Management](Quick Start/File Management.md "FILES, SEARCH")

#### [Work with Mobile and other Devices](Quick Start/Work with Mobile and other Devices.md "MOBILE, IOS, SYNC")
#### [Older macOS Support](Quick Start/Older macOS Support.md "MACOS")

#### [Trouble Shooting](Quick Start/Trouble Shooting.md "FAQ, TUTORIAL")



### How-Tos

#### [Auto Numbering for Headings](How-Tos/Auto Numbering for Headings.md "STYLE")
#### [Code Block Styles/Themes](How-Tos/Code Block Styles Themes.md "STYLE, CODE FENCES")
#### [Custom Fonts](How-Tos/Custom Fonts.md "STYLE, FONT")
#### [Change Background](How-Tos/Change Background.md "STYLE")
#### [Add Custom CSS](How-Tos/Add Custom CSS.md "STYLE, TUTORIAL")
#### [Open in Typora from Sublime (macOS)](How-Tos/Open in Typora from Sublime (macOS).md "SUBLIME, MAC, DEV")
#### [Control TOC Levels](How-Tos/Control TOC Levels.md "STYLE")
#### [Change Styles in Focus Mode](How-Tos/Change Styles in Focus Mode.md "STYLE")
#### [Add Search Service](How-Tos/Add Search Service.md "SEARCH, CONTEXTMENU")
#### [Use Typora from Shell or Cmd](How-Tos/Use Typora from Shell or Cmd.md "DEV, SHELL")
#### [Resize Images](How-Tos/Resize Images.md "IMAGE")
#### [Auto Save](How-Tos/Auto Save.md "AUTO-SAVE")
#### [Version Control and Recovery](How-Tos/Version Control and Recovery.md "AUTO-SAVE, HISTORY, VERSION")
#### [RTL (Right-to-Left) Support (experimental)](How-Tos/RTL (Right-to-Left) Support (experimental).md "RTL")
#### [Task List — Easy Way to Record Todos](How-Tos/Task List Easy Way to Record Todos.md "MARKDOWN, STYLE")
#### [Auto Pair](How-Tos/Auto Pair.md "MARKDOWN")
#### [Page Breaks](How-Tos/Page Breaks.md "MARKDOWN, PAGINATION, CSS, HTML")
#### [Strict Mode](How-Tos/Strict Mode.md "MARKDOWN")
#### [Change Width of Writing Area](How-Tos/Change Width of Writing Area.md "STYLE")
#### [Whitespace and Line Breaks](How-Tos/Whitespace and Line Breaks.md "STYLE, MARKDOWN")
#### [Smart Punctuation (SmartyPants)](How-Tos/Smart Punctuation (SmartyPants).md "MARKDOWN, SMARTYPANTYS, QUOTES, DASHES")
#### [Embed Video, Media or Web Contents](How-Tos/Embed Video, Media or Web Contents.md "HTML, VIDEO, EMBED")
#### [Dark Mode](How-Tos/Dark Mode.md "DARK, THEME, MACOS")
#### [Launch Options](How-Tos/Launch Options.md "LAUNCH, FOLDER, PROJECT")
#### [Word Count](How-Tos/Word Count.md "WORD, STATISTICS")
#### [Focus Mode and Typewriter Mode](How-Tos/Focus Mode and Typewriter Mode.md "VIEW, TYPEWRITER, FOCUS")
#### [Copy and Paste](How-Tos/Copy and Paste.md "COPY, CLIPBOARD")
#### [Zoom](How-Tos/Zoom.md "ZOOM, VIEW, MAGNIFICATION")
#### [Upload Images](How-Tos/Upload Images.md "IMAGE, UPLOAD, CLOUD")
#### [Get Typora Logs](How-Tos/Get Typora Logs.md "LOG, DEBUG, FEEDBACK")
#### [Launch Arguments (Windows / Linux)](How-Tos/Launch Arguments (Windows Linux).md "CMD")



### Reference

#### [Language Support in Code Fences](Reference/Language Support in Code Fences.md "CODE FENCES")
#### [Privacy Policy](Reference/Privacy Policy.md "TERMS AND CONDITIONS")
#### [Translations and Languages Support](Reference/Translations and Languages Support.md "I18N")
#### [Acknowledgement](Reference/Acknowledgement.md "ACKNOWLEDGEMENT, CREDITS, OSS")
#### [End User License Agreement](Reference/End User License Agreement.md "EULA, TERM OF SERVICE")
#### [System Requirements](Reference/System Requirements.md "SYSTEM")



### Contact

#### [Email](Contact/Email.md "hi@typora.io")
#### [Issues](Contact/Issues.md "Typora Issues")
#### [Twitter](Contact/Twitter.md "@typora")