```
RTL
```

# RTL (Right-to-Left) Support (experimental)

August 23, 2017 by typora.io

> About where to put those CSS, please follow [Add Custom CSS](http://support.typora.io/Add-Custom-CSS/).
>
> This is not fully tested, since I’m not a RTL user, if you found any bugs, please report to [hi@typora.io](mailto:hi@typora.io)

Add following Custom CSS:

```
#write {
    direction: rtl;
}
```