```
LAUNCH, FOLDER, PROJECT
```

# Launch Options

December 30, 2018 by typora.io

When typora is launched, you could set default actions to:

- Open new file
- Restore last closed folder
- Restore last closed file and folder
- Open a custom folder.

This option can be set in preferences panel:

![img](https://support.typora.io/media/files/Screen%20Shot%202019-01-06%20at%2022.39.31.png)

hosted on [Github](https://github.com/typora/wiki-website).