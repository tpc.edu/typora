```
DEV, SHELL
```

# Use Typora from Shell or Cmd

September 26, 2016 by typora.io

## macOS

You can use `open -a typora xxx.md` to open the markdown file `xxx.md` in Typora from a command line. If Typora is your default editor for `.md` files, when `open xxx.md` would be enough.

You can also add

```
alias typora="open -a typora"
```

in your `.bash_profile` or other configuration file, then you would be able to simply type `typora xxx.md` instead.

## Windows

If Typora is set as the default markdown editor, then in cmd.exe, then typing `.\example.md` or `start example.md` will open this markdown file.

To set Typora as the default markdown editor:

1. Select one of your markdown files
2. Open context menu and choose
   - *Properties* and then click on *Change* buttong, as shown in **Figure 1**
   - *Open with -> Choose another app*, as shown in **Figure 2**
3. Choose *Typora* or *Typora Launcher* in **How do you want to open this file?** and set a checkmark for *Always use this app to open .md files.*

**Figure 1 - File properties window**
![Figure 1 - Open Properties of Markdown file](http://support.typora.io/media/use-from-shell/Snip20180704_1.png)

**Figure 2 - Application chooser window**
![Figure 2 - Applicaiton chooser](http://support.typora.io/media/use-from-shell/Snip20180704_2.png)

## Linux

After [installing Typora on Linux](http://support.typora.io/Typora-on-Linux/), user can run `typora` directly in terminal, for example:

```
typora ~/Document/test.md
```