```
MARKDOWN
```

# Strict Mode

December 9, 2017 by typora.io

You could enable **strict mode** to tell Typora to follow [GFM’s spec](https://github.github.com/gfm/) more strictly. You could disable it if you want the parser to be more “fault tolerant”.

## Strict Mode

In Strict Mode, if you want to input **headings**, you must put whitespace after the `#` characters, for example (`·` represents for one whitespace):

| Markdown Source | Result in Strict Mode | Result without Strict Mode |
| --------------- | --------------------- | -------------------------- |
| `###Header`     | ###Header             | Header                     |
| `###·Header`    | Header                | Header                     |

In Strict Mode, if you want to create new block/paragraph in a list item, you must input enough whitespace indentation to make the new paragraph strictly align with its previous paragraph, for example (`·` represents one space):

| Strict Mode                                                  | without Strict Mode                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `1.·aaa ··bbb // not in same list ``1.·aaa ···bbb // in same list ``10.·aaa ··1.·ccc // not a sub list ``10.·aaa ····1.·ccc // a sub list ` | `1.·aaa ··bbb // in same list ``1.·aaa ···bbb // in same list ``10.·aaa ··1.·ccc // a sub list ``10.·aaa ··· 1.·ccc // a sub list ` |

## Enable/Disable Strict Mode

This option can be toggled in the `Preference Panel` → `Markdown` section. The option will be applied after Typora has been restarted.

hosted on [Github](https://github.com/typora/wiki-website).