```
MARKDOWN
```

# Auto Pair

August 23, 2017 by typora.io

![Snip20170824_4](http://support.typora.io/media/auto-pairs/Snip20170824_4.png)

## Normal Auto Pair

Open preference panel, and enable “Auto pair brackets and quotes” (item 1 in image above) to turn on normal auto pair, which has the same behavior as most code editors.

## Auto Pair for Extended Chars

If “Auto pair common markdown syntax” is enabled, the auto pair behavior will also be extended to markdown symbols, like `*`, `~`, `\``, or `_`, if “hightlight”, “inline math”, “superscript” is enabled, then auto pair for `=`, `$` and `^` will also be turned on.

Please note that, for `~`, `=` and `^` , the ending pair will not be inserted automatically, but when you select a word, and input characters like `=`, then the word will be surrounded by `=` automatically.