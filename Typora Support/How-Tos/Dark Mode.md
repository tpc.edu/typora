```
DARK, THEME, MACOS
```

# Dark Mode

December 30, 2018 by typora.io

MacOS Mojave introduces “Dark Mode”, it is also fully supported by Typora. You could use it by:

1. Turn on “Dark Mode” in macOS Mojave
2. Choose a dark theme in Typora.

![CleanShot 2018-10-23 at 00.52.51@2x](https://support.typora.io/media/new-59/CleanShot%202018-10-23%20at%2000.52.51@2x.png)

Read more about [Themes](https://support.typora.io/About-Themes/) or find more themes at [Theme Gallery](https://theme.typora.io/).

hosted on [Github](https://github.com/typora/wiki-website).