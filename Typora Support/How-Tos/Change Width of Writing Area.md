```
STYLE
```

# Change Width of Writing Area

March 8, 2018 by typora.io

> Some of following CSS style will work for latest version of Typora (>= 0.9.9.6 on macOS, and >=0.9.13 on Windows).
>
> About where to put those CSS, please follow [Add Custom CSS](http://support.typora.io/Add-Custom-CSS/).

Example CSS:

```
#write {
  max-width: 1800px; /*adjust writing area position*/
}
```

You could also use other css styles like `padding-left` or `padding-right` to adjust the writing area.

To change the width of source code mode:

```
#typora-source .CodeMirror-lines {
  max-width: auto; /*or 1000px*/
}
```

hosted on [Github](https://github.com/typora/wiki-website).