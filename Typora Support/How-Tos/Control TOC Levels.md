```
STYLE
```

# Control TOC Levels

June 25, 2016 by typora.io

> TIP: To know where to put those CSS snippets, please see [Add Custom CSS](http://support.typora.io/Add-Custom-CSS/).

In Typora `[TOC]` will generate “Table of Contents”. By default it will show headings from `h1` to `h6`. To hide lower level headings, for instance, `h6`, you would append this CSS:

```
.md-toc-h6 {
  display: none;
}
```