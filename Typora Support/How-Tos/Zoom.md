```
ZOOM, VIEW, MAGNIFICATION
```

# Zoom

July 26, 2019 by typora.io

Zoom function is now enable on all supported platforms. You could zoom in / out the view from `view` menu or press the corresponding shortcut keys (which are displayed in right side go menu item).

![img](https://support.typora.io/media/zoom/Screen%20Shot%202019-07-26%20at%2001.22.24.png) ![img](https://support.typora.io/media/zoom/Screen%20Shot%202019-07-26%20at%2001.33.02.png)

When view is zoomed, the zoom hint panel will show on the right top of current window, and you can adjust or reset zoom factor there.

On Later macOS version, you could use two finger gesture in touchpad to zoom in / out the whole view, just like other touch devices. To disable this, please uncheck `Allow Magnification` under `view` menu.



hosted on [Github](https://github.com/typora/wiki-website).