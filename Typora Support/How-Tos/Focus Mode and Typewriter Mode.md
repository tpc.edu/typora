```
VIEW, TYPEWRITER, FOCUS
```

# Focus Mode and Typewriter Mode

January 5, 2019 by typora.io

## Focus Mode

When “Focus Mode” is enabled, Typora will fade out other contents except current line/block. You could turn “Focus Mode” on/off from `view` menu.

|                      with `Focus Mode`                       |                     without `Focus Mode`                     |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![img](https://support.typora.io/media/focus-and-typewriter/Screen%20Shot%202019-01-05%20at%2015.50.26.png) | ![img](https://support.typora.io/media/focus-and-typewriter/Screen%20Shot%202019-01-05%20at%2015.50.48.png) |

## Typewriter Mode

Typewriter mode mimics the behavior of mechanic typewriters — it scrolls the article to keep current caret fixed when typing.

You could turn on/off typewriter mode from `view` menu.

By default, it will *always* keep caret in middle of the window even when you change selection by mouse click. If you only want to use fixed scrolling when typing, you could disable this behavior from `preferences panel` → `Always keep caret in middle of screen\nwhen typewriter mode is enabled`.

hosted on [Github](https://github.com/typora/wiki-website).