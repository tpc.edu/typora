```
SYSTEM
```

# System Requirements

January 16, 2019 by typora.io

Typora works on

## Operation System

- **macOS**: ≥ v10.10 (We may end support for macOS)
- **Linux**: tested on Ubuntu 16.04 and 18.04, support (x64) architecture (32 bit is no longer support on Linux).
- **Windows**: require Windows 7, Windows 8 or Windows 10, support x86 and amd64 (x64) architecture.

## Hardware

- Windows An Intel Pentium 4 processor or later that’s SSE2 capable 1G of RAM
- Mac An Intel processor that’s 64-bit 1G of RAM
- Linux An Intel Pentium 4 processor or later that’s SSE2 capable

hosted on [Github](https://github.com/typora/wiki-website).