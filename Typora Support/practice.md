[TOC]

# HTML Support in Typora

### Inline HTML

> <span style='color:red'>This is red</span>
>
> <ruby>漢<rt>ㄏㄢˋ</rt></ruby>
>
> <kbd>Ctrl</kbd>+<kbd>F9</kbd>
>
> <span style='font-size:2rem; background:yellow;'>**Bigger**</span>
>
> HTML entities like &reg; &#182;
>
> If needed, you could use inline HTML tags directly, for example:
>
> Press <kbd>Command</kbd>+<kbd>O</kbd> to open a <span style='background:black;color:white;font-family:monospace'>markdown</span> file.
>
> ## <a name='anchor'></a> Header 2
>
> <span style='display:none'>I am hidden after export</span>

### HTML Entities

>  &frac14; → ¼,&#x1D517; → 𝔗

### HTML Block

<details>
    <summary>I have keys but no locks. I have space but no room, You can enter but can't leave. What am I?</summary>
    A keyboard.
</details>

### Media and Embedded Contents

#### Video

<video src='xxx.mp4'>

#### Audio

<audio src='xxx.mp3'>

#### Embed Web Contents

<iframe height='265' scrolling='no' title='Fancy Animated SVG Menu' src='//codepen.io/jeangontijo/embed/OxVywj/?height=265&theme-id=0&default-tab=css,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/jeangontijo/pen/OxVywj/'>Fancy Animated SVG Menu</a> by Jean Gontijo (<a href='https://codepen.io/jeangontijo'>@jeangontijo</a>) on <a href='https://codepen.io'>CodePen</a>. </iframe>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over <a href="https://twitter.com/GrandTetonNPS?ref_src=twsrc%5Etfw">@GrandTetonNPS</a>. <a href="https://twitter.com/hashtag/nature?src=hash&amp;ref_src=twsrc%5Etfw">#nature</a> <a href="https://twitter.com/hashtag/sunset?src=hash&amp;ref_src=twsrc%5Etfw">#sunset</a> <a href="http://t.co/YuKy2rcjyU">pic.twitter.com/YuKy2rcjyU</a></p>&mdash; US Department of the Interior (@Interior) <a href="https://twitter.com/Interior/status/463440424141459456?ref_src=twsrc%5Etfw">May 5, 2014</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Comments

<!-- I am some comments not end, not end... here the comment ends -->

* test drag file from sidebar to writing area . [Typora Support.md](Typora Support.md) 
* <img src="http://support.typora.io/media/auto-save/general.png" style="zoom:70%;height:500px;width:500px;float:right" />

# Header1

## Header2.1

## Header2.2

### Header3.1

### Header3.2

## Header 2.3

### Header3.1

#### Header4.1

##### Header5.1

###### Header6.1

- [ ] 待办任务
- [x] 完成任务





