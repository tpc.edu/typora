---
gist: PANDOC, TUTORIAL, DOCX
---

[toc]

# Pandoc Integration

### What is Pandoc

[Pandoc](http://pandoc.org/) is a universal document text converter. Typora uses it to support file import/export features for several file types.

### Install Pandoc

#### For Mac Users

##### Install from downloaded package installer

##### Install from Homebrew



#### For Windows Users

Download the `pandoc-*-window.msi` from Pandoc’s [download page](https://github.com/jgm/pandoc/releases/latest), open it and follow the instructions for installation.

![Install Pandoc](http://support.typora.io/media/pandoc/pandoc-win.PNG "Pandoc Setup")

### Use Pandoc

After Pandoc is installed, then you can import supported file types by clicking File -> Import from the menu bar, or simply drag and drop a file into Typora. There are also new Export functions available from the menu bar. Pandoc will run in background for those tasks and then exit automatically, so you may not notice it.

### FAQ

#### Which version of Pandoc is supported?

#### Can Typora work without Pandoc?

#### Which file types can be imported or exported by Typora?

#### What’s the difference between exporting by Typora and exporting by using Pandoc from command line?

#### Can all block/inline element types be exported correctly?

#### How to uninstall Pandoc for mac?

#### Found a bug and unsupported syntax for exporting?

