---
title: MACOS
---

# Older macOS Support

August 3, 2019 by typora.io

The latest version of Typora supports macOS ≥ 10.13, for older macOS, 10.11 and 10.12 are not in active support. Support for macOS ≤ 10.10 has ended.

You could find and download available working versions here:

- **macOS ≥ 10.13** [latest version](https://typora.io/download/Typora.dmg)
- **macOS 10.11 and 10.12** [0.9.9.26.7 beta](https://typora.io/download/Typora-0.9.9.26.7.dmg) (discontinued)
- **macOS 10.10** [0.9.9.25.3 beta](https://typora.io/download/Typora-0.9.9.25.3.dmg) (discontinued)