---
name: YAML Front Matter (YAML前页)
gist: MARKDOWN, TUTORIAL
typora-root-url: /User/Abner/Website/typora.io
---

[toc]

# Markdown Reference

## Overview

Markdown is created by Daring Fireball; the original guideline is here. Its syntax, however, varies between different parsers or editors. Typora try to follow GitHub Flavored Markdown, but may still have small incompatibilities.

## Block Elements

### Paragraph and line breaks



### Headers

> # This is an H1
>
> ## This is an H2
>
> ###### This is an H6

### Blockquotes

> This is a blockquote with two paragraphs. This is first paragraph.
>
> This is second pragraph. Vestibulum enim wisi, viverra nec, fringilla in, laoreet viea, risus.

### Lists

* Red

* Green

* Blue

1. Red
2. Green
3. Blue

### Task List

- [ ] task list item
- [ ] list syntax required
- [ ] normal **formatting**, @mentions, #1234 refs
- [ ] incomplete
- [x] completed

### (Fenced)Code Blocks

```
function test() {
  console.log("notice the blank line before this function?");
}
```
``` ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

### Math Blocks

$$
\mathbf{V}_1 \times \mathbf{V}_2 =  
\begin{vmatrix}
\mathbf{i} & \mathbf{j} & \mathbf{k} \\
\frac{\partial X}{\partial u} &  \frac{\partial Y}{\partial u} & 0 \\
\frac{\partial X}{\partial v} &  \frac{\partial Y}{\partial v} & 0 \\
\end{vmatrix}
$$

You can find more details [here](http://support.typora.io/Math/).

### Tables

| Left-Aligned | Center Aligned  | Right Aligned                            |
| ------------ | --------------- | ---------------------------------------- |
| col 3 is     | some wordy text | [i1](https://baidu.com "123")$1600[^fn1] |



### Footnotes

You can create footnotes like this[^fn1] and this[^fn2].

[^fn1]: Here is the *text* of the first **footnote**.
[^fn2]: Here is the *text* the second **fontnote**.

### Horizontal Rules

***

---

___

### YAML Front Matter

Enter `---` at the top of the article and then press `Return` to introduce a metadata block. Alternatively, you can insert a metadata block from the top menu of Typora.

### Table of Contents(TOC)

Enter `[toc]` and press the `Return` key to create a “Table of Contents” section. The TOC extracts all headers from the document, and its contents are updated automatically as you add to the document.

## Span Elements

### Links

#### Inline Links

This is [an example](http://example.com/ "Title") inline link.

[This link](http://example.net "link title") has no title attribute.

#### Internal Links

Hold down Cmd (on Windows: Ctrl) and click on [this link](#block-elements) to jump to header `Block Elements`.

#### Reference Links

This is [an example][id] reference-style link.
Then, anywhere in the document, you define your link label on a line by itself like this:

[id]: http://example.com/  "Optional Title Here"

[Google][]
And then define the link:

[Google]: http://google.com/

### URLs

<i@typora.io>

www.google.com

### Images

![Alt text](/path/to/img.jpg) ![Alt text](/path/to/img.jpg "Optional title")

![alt-1](/blog/img/test.png)

![alt](E:\life\photo\wallpaper\窃·格瓦拉.jpg)

### Emphasis

*single asterisks*

_single underscores_

\*this text is surrounded by literal asterisks\*

### Strong

**double asterisks**

__double underscores__

### Code

Use the `printf()` function.

### Strikethrough

~~Mistaken text.~~

### Emoji:happy:

Enter enoji with syntax :smile_cat:. To make it easiter, an auto-complete helper will pop up after typing : and the start of an emoji name. 

### Inline Math

To use this feature, please enable it first in the` Markdown` tab of the preference panel. Then, use` $` to wrap a LaTeX command. For example: `$\lim_{x \to \infty} \exp(-x) = 0$`.

$\lim-{x \to \infty} \exp(-x)$.

### SubScript

To use this feature, please enable it first in the `Markdown` tab of the preference panel. Then, use` ~` to wrap subscript content. For example: `H~2~O`, `X~long\ text~`/

H~2~O, X~long\ text~/

### Superscript

To use this feature, please enable it first in the `Markdown` tab of the preference panel. Then, use `^` to wrap superscript content. For example: `X^2^`

X^2^.

### Highlight

To use this feature, please enable it first in the `Markdown` tab of the preference panel. Then, use `==` to wrap highlight content. For example: `==highlight==`

==highlight==.

## HTML

You can use HTML to style content what pure Markdown does not support. For example, use <span style="color:red">this text is red</span> to add text with red color.

### Underlines

Underline isn’t specified in Markdown of GFM, but can be produced by using underline HTML tags:

<u>Underline</u> becomes Underline.

### Embed Contents

Some websites provide iframe-based embed code which you can also paste into Typora. For example:

<iframe height='265' scrolling='no' title='Fancy Animated SVG Menu' src='http://codepen.io/jeangontijo/embed/OxVywj/?height=265&theme-id=0&default-tab=css,result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'></iframe>

### Video

You can use the `<video>` HTML tag to embed videos. For example:

<video src="xxx.mp4" />

### Other HTML Support

You can find more details [here](http://support.typora.io/HTML/).
